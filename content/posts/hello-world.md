+++
title = "Hello World!"
description = "First post on the new blog"
tags = []
date = "2016-05-11"
categories = ["Posts"]
slug = "hello-world"
+++

The inaugural post of the blog I decided to start writing as sort of a log of my various projects and hobbies.

I'd like to think I can keep to a schedule of 1-3 posts per week, I'm not too concerned about length. It's more about getting myself into the habit
of writing posts.